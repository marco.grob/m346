# Infrastructure As Code

[TOC]

Infrastruktur als Code ist ein Paradigma (grundsätzliche Denkweise) zur Infrastruktur-Automation.

Es basiert auf konsistenten und wiederholbaren Definitionen (Code) für die Bereitstellung von Systemen und deren Konfiguration.

Produkte sind u.a. Cloud-init, Vagrant, TerraForm, CLIs etc. 

#### Definition von Infrastruktur als Code

- Infrastruktur als Code ist ein Ansatz zur Automatisierung der Infrastruktur, der auf Praktiken aus der Softwareentwicklung basiert. 
- Dabei werden konsistente, wiederholbare Prozesse für die Bereitstellung und Änderung von Systemen und deren Konfiguration verwendet.
- Änderungen werden an Deklarationen (Code) vorgenommen und dann durch automatisierte Prozesse, auf Systeme übertragen.
- Infrastruktur als Code hat sich in den anspruchsvollsten Umgebungen bewährt. Für Unternehmen wie Amazon, Netflix, Google und Facebook sind IT-Systeme nicht nur geschäftskritisch. Sie sind das Geschäft!

#### Ziele von Infrastruktur als Code

- Die IT-Infrastruktur unterstützt und ermöglicht Veränderungen, anstatt ein Hindernis oder eine Einschränkung zu sein.
- Änderungen am System sind Routine, ohne Drama oder Stress für Benutzer oder IT-Mitarbeiter.
- IT-Mitarbeiter verbringen ihre Zeit mit wertvollen Dingen, die ihre Fähigkeiten einbeziehen, und nicht mit sich wiederholenden Routineaufgaben.
- Benutzer können die benötigten Ressourcen definieren, bereitstellen und verwalten, ohne dass IT-Mitarbeiter dies für sie tun müssen.
- Teams können sich einfach und schnell von Fehlern erholen, anstatt davon auszugehen, dass Fehler vollständig verhindert werden können.
- Verbesserungen werden kontinuierlich vorgenommen und nicht durch teure und riskante „Urknall“ -Projekte.
- Lösungen für Probleme werden durch Implementierung, Test und Messung bewiesen, anstatt sie in Besprechungen und Dokumenten zu diskutieren.

### YAML

YAML ist eine einfache Textdatei mit einer definierten Syntax, die wir folgend verwenden werden. Es ist einfach zum Lesen. 

<https://yaml.org/>: YAMLs offizielle Seite. Hier finden Sie auch den Link auf die Spezifikation.

Das folgende Beispiel von der YAML Webseite zeigt die wichtigsten Elemente. Sie haben grundsätzlich die Elemente *Collections* und *Scalars*, die unterschiedlich gemappt werden (z.B. key-value-pairs oder Aufzählungen). 

![yaml](./x_gitres/yaml.png)

### Cloud-init

Cloud-init bietet eine Konfiguration des Betriebssystem während der Installation. Stellen Sie sich folgendes Szenario vor. Sie müssen 10 Server installieren mit verschiedenen Betriebssystemen (Ubuntu, Redhat). Alle benötigen die gleiche Konfiguration betreffend Software/Paketen.  Sie haben die folgenden Optionen:

- Sie installieren alle Server einzeln und installieren alle Pakete und führen die verschiedenen Befehle aus.
- Sie erstellen ein Bash-Script, welches alle Befehle ausführt. Sie müssen es nur noch ausführen. Trotzdem haben Sie noch den Aufwand das Skript auf alle Server zu laden und auszuführen.
- Sie erstellen eine Konfiguration-Datei und geben diese bei der Installation der Instanz mit. Keine weiteren Schritte sind notwendig. Dies ist genau das, was Cloud-init ihnen bietet.

Es wird hier klar, welche Variante bevorzugt wird. Die Automatisierung kann natürlich noch weiter getrieben werden, aber dies kommt in einem späteren Kapitel.

Die Referenz zu Cloud-init Dateien ist umfangreich. Sie werden in der Kompetenz KN02 die wichtigsten Elemente und Konfigurationen kennenlernen und anwenden.

<https://cloudinit.readthedocs.io/en/latest/>: Cloud-inits offzielle Seite. 

### Command Line Interfaces

TBD

### TerraForm

TBD

### Quellen

Infrastructure As Code: [Cloud Native Kurs TBZ/HF](https://gitlab.com/ch-tbz-hf/Stud/cnt/-/tree/main/2_Unterrichtsressourcen/B)

Screenshot YAML: [Offizielle Webseite](https://yaml.org/spec/1.2.2/#21-collections)





