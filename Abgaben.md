# Umsetzung / Abgaben

[TOC]

### Git Repository

Erstellen Sie ein Gitlab oder Github repository und fügen Sie die Lehrperson(en) mit Leserechten hinzu. Sie werden alle Kompetenzen in diesem Git Repo abgeben. Planen Sie daher ihre Struktur bereits entsprechend, z. B.

KN01/&lt;Ihre Dateien hier &gt;

KN02/&lt;Ihre Dateien hier &gt;

....

### Vorgehen

Sie werden meistens praktisch in Kompetenzthemen arbeiten. Dass Sie ein Thema verstanden haben, werden Sie beweisen, indem Sie ihre Schritte **und/oder** Endprodukte mit Screenshots belegen und im Git Repository ablegen. **Verwenden Sie Markdown**! 

**ACHTUNG**: In den meisten Fällen erstellen Sie erst Screenshots, nachdem bei Ihnen alles einwandfrei läuft. Dies um zu verhindern, dass sie die gleichen Bilder mehrmals kreieren.

Sie dürfen jeweils in 2er Gruppen arbeiten, aber **jeder** Teilnehmer führt **alle** Schritte durch und erstellt eine **eigene** Dokumentation/Abgabe.

### Interpretation der Fragestellung

Wenn in Kompetenzen "Zeigen Sie, dass..." steht, ist damit gemeint, dass Sie die Schritte dokumentieren sollen (in Markdown) und zwar so, dass aus den Screenshots und Text klar wird, welche Aktionen Sie durchgeführt haben.

**Denken Sie bitte mit bei der Abgabe/den Screenshots**. Wenn Sie z.B. einen Screenshot einer Webseite erstellen, aber die URL fehlt, die zeigt, was Sie eigentlich aufrufen, sagt der Screenshot wenig aus.  

